package com.geiliwang.web.enums;

/**
 * 标属性枚举
 */
public enum PlatformAttrEnum {
    STATEOWNEDENTERPRISE("STATEOWNEDENTERPRISE","国企背景"),
    PRIVATE("PRIVATE","民营糸"),
    VENTURECAPITAL("VENTURECAPITAL","有风投"),
    OFFLINE("OFFLINE","线下业务"),
    ONEMOREYEAR("ONEMOREYEAR","一年以上"),
    IPO("IPO","上市背景"),
    ;

    PlatformAttrEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private String code;

    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
