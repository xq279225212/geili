package com.geiliwang.web.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties(prefix = "sms-config")
public class SmsConfig {
    String AccessId = "LTAIj9YD3l0huVLO";
    String accessKey = "DzWylduSjJ8ghTFNOv8iklaKRV2eKV";
    String MNSEndpoint = "http://1086280219856877.mns.cn-hangzhou.aliyuncs.com/";//短信接入地址
    String Topic = "sms.topic-cn-hangzhou";    //短信主题
    String signName = "阿里云短信测试专用";

    public String getAccessId() {
        return AccessId;
    }

    public void setAccessId(String accessId) {
        AccessId = accessId;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public void setAccessKey(String accessKey) {
        this.accessKey = accessKey;
    }

    public String getMNSEndpoint() {
        return MNSEndpoint;
    }

    public void setMNSEndpoint(String MNSEndpoint) {
        this.MNSEndpoint = MNSEndpoint;
    }

    public String getTopic() {
        return Topic;
    }

    public void setTopic(String topic) {
        Topic = topic;
    }

    public String getSignName() {
        return signName;
    }

    public void setSignName(String signName) {
        this.signName = signName;
    }
}
