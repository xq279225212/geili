package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public class Platformscheme implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 平台id
     */
    @TableField("platformId")
    private Long platformId;
    /**
     * 项目名称
     */
    @TableField("projectName")
    private String projectName;
    /**
     * 投资区间
     */
    @TableField("invesBelong")
    private String invesBelong;
    /**
     * 返利
     */
    @TableField("investRebate")
    private String investRebate;
    /**
     * 是否可用红包
     */
    @TableField("usedRed")
    private String usedRed;
    /**
     * 平台利息
     */
    @TableField("platformInterest")
    private String platformInterest;
    /**
     * 总收益
     */
    private String earning;
    /**
     * 删除标志
     */
    @TableField("delFlag")
    private String delFlag;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

    public String getInvesBelong() {
        return invesBelong;
    }

    public void setInvesBelong(String invesBelong) {
        this.invesBelong = invesBelong;
    }

    public String getInvestRebate() {
        return investRebate;
    }

    public void setInvestRebate(String investRebate) {
        this.investRebate = investRebate;
    }

    public String getUsedRed() {
        return usedRed;
    }

    public void setUsedRed(String usedRed) {
        this.usedRed = usedRed;
    }

    public String getPlatformInterest() {
        return platformInterest;
    }

    public void setPlatformInterest(String platformInterest) {
        this.platformInterest = platformInterest;
    }

    public String getEarning() {
        return earning;
    }

    public void setEarning(String earning) {
        this.earning = earning;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Platformscheme{" +
        ", id=" + id +
        ", platformId=" + platformId +
        ", projectName=" + projectName +
        ", invesBelong=" + invesBelong +
        ", investRebate=" + investRebate +
        ", usedRed=" + usedRed +
        ", platformInterest=" + platformInterest +
        ", earning=" + earning +
        ", delFlag=" + delFlag +
        "}";
    }
}
