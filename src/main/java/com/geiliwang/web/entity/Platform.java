package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public class Platform implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    private String platformName;
    /**
     * logo图片路径
     */
    @TableField("logoPath")
    private String logoPath;
    /**
     * 电话
     */
    private String phone;
    /**
     * 上线时间
     */
    @TableField("onlineDate")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date onlineDate;
    /**
     * 数据确认方式
     */
    @TableField("dataConfirm")
    private String dataConfirm;
    /**
     * 标属性
     */
    private String platformAttr;
    /**
     * 年化收益
     */
    private String annualized;
    /**
     * 投资期限
     */
    @TableField("investDate")
    private String investDate;
    /**
     * 投资金额
     */
    @TableField("investAmount")
    private String investAmount;
    /**
     * 返利
     */
    private String rebate;
    /**
     * 平台地区
     */
    @TableField("areaId")
    private Integer areaId;
    /**
     * 投标次数
     */
    @TableField("investCount")
    private String investCount;
    /**
     * 提示
     */
    private String remark;
    @TableField("gmtCreate")
    private Date gmtCreate;
    /**
     * 删除标志
     */
    @TableField("delFlag")
    private String delFlag;
    /**
     * 万元总收益
     */
    @TableField("perEarn")
    private String perEarn;
    /**
     * 是否置顶
     */
    private String isTop;
    /**
     * 平台跳转链接
     */
    private String platformurl;

    public String getPlatformName() {
        return platformName;
    }

    public void setPlatformName(String platformName) {
        this.platformName = platformName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLogoPath() {
        return logoPath;
    }

    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Date getOnlineDate() {
        return onlineDate;
    }

    public void setOnlineDate(Date onlineDate) {
        this.onlineDate = onlineDate;
    }

    public String getDataConfirm() {
        return dataConfirm;
    }

    public void setDataConfirm(String dataConfirm) {
        this.dataConfirm = dataConfirm;
    }

    public String getPlatformAttr() {
        return platformAttr;
    }

    public void setPlatformAttr(String platformAttr) {
        this.platformAttr = platformAttr;
    }

    public String getAnnualized() {
        return annualized;
    }

    public void setAnnualized(String annualized) {
        this.annualized = annualized;
    }

    public String getInvestDate() {
        return investDate;
    }

    public void setInvestDate(String investDate) {
        this.investDate = investDate;
    }

    public String getInvestAmount() {
        return investAmount;
    }

    public void setInvestAmount(String investAmount) {
        this.investAmount = investAmount;
    }

    public String getRebate() {
        return rebate;
    }

    public void setRebate(String rebate) {
        this.rebate = rebate;
    }

    public Integer getAreaId() {
        return areaId;
    }

    public void setAreaId(Integer areaId) {
        this.areaId = areaId;
    }

    public String getInvestCount() {
        return investCount;
    }

    public void setInvestCount(String investCount) {
        this.investCount = investCount;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    public String getPerEarn() {
        return perEarn;
    }

    public void setPerEarn(String perEarn) {
        this.perEarn = perEarn;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public String getPlatformurl() {
        return platformurl;
    }

    public void setPlatformurl(String platformurl) {
        this.platformurl = platformurl;
    }

    @Override
    public String toString() {
        return "Platform{" +
        ", id=" + id +
        ", logoPath=" + logoPath +
        ", phone=" + phone +
        ", onlineDate=" + onlineDate +
        ", dataConfirm=" + dataConfirm +
        ", platformAttr=" + platformAttr +
        ", annualized=" + annualized +
        ", investDate=" + investDate +
        ", investAmount=" + investAmount +
        ", rebate=" + rebate +
        ", areaId=" + areaId +
        ", investCount=" + investCount +
        ", remark=" + remark +
        ", gmtCreate=" + gmtCreate +
        ", delFlag=" + delFlag +
        ", perEarn=" + perEarn +
        "}";
    }
}
