package com.geiliwang.web.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Platformscheme;
import com.geiliwang.web.service.PlatformschemeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Controller
@RequestMapping("/manage/platformscheme")
public class PlatformschemeController {

    @Autowired
    private PlatformschemeService platformschemeService;

    @RequestMapping("/toAdd")
    public String toAdd(@RequestParam("id") Long platformId,Model model){
        model.addAttribute("platformId",platformId);
        return "/pages/platform/scheme-add";
    }

    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<String> add(Platformscheme platformscheme){
        boolean flag = platformschemeService.insert(platformscheme);
        if(flag){
            return ResponseEntity.ok("保存成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseEntity<String> delete(@RequestParam("id")Long id){
        Platformscheme platformscheme = platformschemeService.selectById(id);
        if(null != platformscheme) {
            platformscheme.setDelFlag("1");
            boolean flag = platformschemeService.updateById(platformscheme);
            if (flag) {
                return ResponseEntity.ok("删除成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除失败");
    }

    @RequestMapping("/toList")
    public String toList(@RequestParam("id") Long platformId,Model model){
        EntityWrapper<Platformscheme> ew = new EntityWrapper<>();
        ew.where("delFlag='0'").eq("platformId",platformId);
        List<Platformscheme> platformschemeList = platformschemeService.selectList(ew);
        model.addAttribute("platformschemeList",platformschemeList);
        return "/pages/platform/scheme-list";
    }

}

