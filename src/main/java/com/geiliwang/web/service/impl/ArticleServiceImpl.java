package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Article;
import com.geiliwang.web.entity.ArticleQuery;
import com.geiliwang.web.mapper.ArticleMapper;
import com.geiliwang.web.service.ArticleService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 文章管理 服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class ArticleServiceImpl extends ServiceImpl<ArticleMapper, Article> implements ArticleService {

    @Autowired
    private ArticleMapper articleMapper;

    @Override
    public PageInfo<Article> queryBy(ArticleQuery query) {
        EntityWrapper<Article> ew = new EntityWrapper<>();
        ew.where("delFlag='0'").orderBy("gmtCreate",false);
        if(StringUtils.isNoneBlank(query.getTitle())){
            ew.like("title",query.getTitle());
        }
        if(StringUtils.isNoneBlank(query.getType())){
            ew.eq("type",query.getType());
        }
        PageHelper.startPage(query.getPageNum(),query.getPageSize());
        List<Article> articleList = articleMapper.selectList(ew);
        return new PageInfo<>(articleList);
    }


    @Override
    public boolean deleteById(Long id) {
        Article article = articleMapper.selectById(id);
        if(null != article){
            article.setDelFlag("1");
            int result = articleMapper.updateById(article);
            return result > 0;
        }
        return false;
    }
}
