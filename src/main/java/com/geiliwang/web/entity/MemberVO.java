package com.geiliwang.web.entity;

public class MemberVO extends Member {

    private String inviteEmail;

    public String getInviteEmail() {
        return inviteEmail;
    }

    public void setInviteEmail(String inviteEmail) {
        this.inviteEmail = inviteEmail;
    }
}
