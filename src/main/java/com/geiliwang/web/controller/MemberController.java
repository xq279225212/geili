package com.geiliwang.web.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.*;
import com.geiliwang.web.service.MemberService;
import com.geiliwang.web.service.PlatformService;
import com.geiliwang.web.service.PlatformorderService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Controller
@RequestMapping(value = {"manage/member","/member"})
public class MemberController {

    @Autowired
    private MemberService memberService;
    @Autowired
    private PlatformorderService platformorderService;
    @Autowired
    private PlatformService platformService;

    @RequestMapping("/toMemberInfo")
    public String toMemberInfo(Model model,HttpServletRequest request){
        Member member = (Member) request.getSession().getAttribute("member");
        if(null != member){
            //体验记录
            List<Platformorder> platformorderList =platformorderService.findByMemberId(member.getId());
            if(!CollectionUtils.isEmpty(platformorderList)){
                List<PlatformorderVo> platformorderVoList = platformorderList.stream().map(order->{
                    PlatformorderVo platformorderVo = new PlatformorderVo();
                    BeanUtils.copyProperties(order,platformorderVo);
                    platformorderVo.setPlatform(platformService.selectById(order.getPlatformId()));
                    return platformorderVo;
                }).collect(Collectors.toList());
                model.addAttribute("platformorderList",platformorderVoList);
            }
            //邀请记录
            EntityWrapper<Member> ew = new EntityWrapper<>();
            ew.eq("inviteId",member.getId());
            List<Member> inviteList = memberService.selectList(ew);
            model.addAttribute("member",member);
            model.addAttribute("inviteList",inviteList);

            //计算注册天数
            Long registerDay = ChronoUnit.DAYS.between(member.getRegisterTime().toInstant(), new Date().toInstant());
            model.addAttribute("registerDay",registerDay+1);
            return "/front/user-index";
        }
        return "/front/user-login";
    }

    /**
     * 完善信息
     * @param member
     * @return
     */
    @RequestMapping("/completInfo")
    public String completInfo(Member member,Model model,HttpServletRequest request){
        Member oldMember = (Member)request.getSession().getAttribute("member");
        if(null != oldMember) {
            oldMember.setSex(member.getSex());
            oldMember.setQq(member.getQq());
            oldMember.setAlipayAccount(member.getAlipayAccount());
            oldMember.setAlipayName(member.getAlipayName());
            boolean flag = memberService.updateById(oldMember);
            if(flag){
                return "/front/user-index";
            }
        }
        return "404";
    }

    /**
     * 修改密码
     * @return
     */
    @RequestMapping("/modifyPwd")
    public String modifyPasswd(HttpServletRequest req){
        Member member =(Member)req.getSession().getAttribute("member");
        if(null != member){
            String oldPwd = req.getParameter("opassword");
            String newPwd = req.getParameter("password");
            String oldPwdEncypt = new String(DigestUtils.md5(oldPwd));
            if(StringUtils.equals(oldPwdEncypt,member.getPasswd())){
                newPwd = new String(DigestUtils.md5(newPwd));
                member.setPasswd(newPwd);
                boolean flag = memberService.updateById(member);
                if(flag){
                    //跳转登陆
                    return "redirect:/toLogin";
                }
            }
        }
        return "404";
    }

    @RequestMapping("/toList")
    public String toList(MemberQuery query,Model model){
        PageInfo<Member> memberPageInfo = memberService.queryBy(query);
        model.addAttribute("memberPageInfo",memberPageInfo);
        model.addAttribute("query",query);
        return "/pages/member/member-list";
    }

    /**
     * 查询邀请记录
     * @param email
     * @param model
     * @return
     */
    @RequestMapping("/toInviteList")
    public String toInviteList(@RequestParam(value = "email",required = false)String email,Model model){
        List<MemberVO> memberVOList = memberService.queryInvites(email);
        model.addAttribute("email",email);
        model.addAttribute("memberVOList",memberVOList);
        return "/pages/member/invite-list";
    }

    @RequestMapping("/logout")
    public String logout(HttpServletRequest request){
        request.getSession().removeAttribute("member");
        return "redirect:/";
    }

}

