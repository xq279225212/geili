package com.geiliwang.web.controller;


import com.geiliwang.web.entity.Banner;
import com.geiliwang.web.entity.BannerQuery;
import com.geiliwang.web.service.BannerService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * banner管理 前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Controller
@RequestMapping("/manage/banner")
public class BannerController {

    @Autowired
    private BannerService bannerService;

    @RequestMapping("/toList")
    public String toList(BannerQuery query, Model model){
        PageInfo<Banner> bannerPageInfo = bannerService.queryBy(query);
        model.addAttribute("query",query);
        model.addAttribute("bannerPageInfo",bannerPageInfo);
        return "/pages/banner/banner-list";
    }

    @RequestMapping("/update")
    @ResponseBody
    public ResponseEntity<String> update(Long id){
        Banner banner = bannerService.selectById(id);
        if(null != banner){
            if(StringUtils.equals("0",banner.getIsUsed())){
                banner.setIsUsed("1");
            }else if(StringUtils.equals("1",banner.getIsUsed())){
                banner.setIsUsed("0");
            }
            boolean flag = bannerService.updateById(banner);
            if(flag){
                return ResponseEntity.ok("修改成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("修改失败");
    }

    @RequestMapping("/toAdd")
    public String toAdd(){
        return "/pages/banner/banner-add";
    }

    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<String> add(Banner banner){
        boolean flag = bannerService.insert(banner);
        if(flag){
            return ResponseEntity.ok("保存成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }

    /**
     * 上传图片
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public Map<String,String> uploadFile(MultipartFile file, HttpServletRequest request) {
        Map result = new HashMap();
        try {
            //上传文件路径
            String uploadPath = request.getServletContext().getRealPath("/upload/banner/");
            //上传文件名
            String filename = file.getOriginalFilename();
            filename = System.currentTimeMillis()+filename;
            File filepath = new File(uploadPath,filename);
            //判断路径是否存在，如果不存在就创建一个
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中

            file.transferTo(new File(uploadPath + File.separator + filename));
            result.put("imgPath","/upload/banner/"+filename);
            System.out.println(file);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("imgPath","");
        }
        return result;
    }
}

