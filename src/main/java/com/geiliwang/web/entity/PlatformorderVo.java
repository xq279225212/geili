package com.geiliwang.web.entity;

public class PlatformorderVo extends Platformorder {

    private Platform platform;

    public Platform getPlatform() {
        return platform;
    }

    public void setPlatform(Platform platform) {
        this.platform = platform;
    }
}
