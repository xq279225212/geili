package com.geiliwang.web.controller;


import com.geiliwang.web.entity.FriendLink;
import com.geiliwang.web.entity.FriendQuery;
import com.geiliwang.web.service.FriendLinkService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * <p>
 * 友情链接 前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Controller
@RequestMapping("/manage/friendLink")
public class FriendLinkController {

    @Autowired
    private FriendLinkService friendLinkService;

    @RequestMapping("/toList")
    public String toList(FriendQuery friendQuery,Model model){
        PageInfo<FriendLink> friendLinkPageInfo = friendLinkService.queryBy(friendQuery);
        model.addAttribute("query",friendQuery);
        model.addAttribute("friendLinkPageInfo",friendLinkPageInfo);
        return "/pages/friendLink/friend-list";
    }

    @RequestMapping("/update")
    @ResponseBody
    public ResponseEntity<String> update(Long id){
        FriendLink friendLink = friendLinkService.selectById(id);
        if(null != friendLink){
            if(StringUtils.equals("0",friendLink.getIsUsed())){
                friendLink.setIsUsed("1");
            }else if(StringUtils.equals("1",friendLink.getIsUsed())){
                friendLink.setIsUsed("0");
            }
            boolean flag = friendLinkService.updateById(friendLink);
            if(flag){
                return ResponseEntity.ok("修改成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("修改失败");
    }

    @RequestMapping("/toAdd")
    public String toAdd(){
        return "/pages/friendLink/friend-add";
    }

    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<String> add(FriendLink friendLink){
        boolean flag = friendLinkService.insert(friendLink);
        if(flag){
            return ResponseEntity.ok("保存成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseEntity<String> delete(@RequestParam("id")Long id){
        boolean flag = friendLinkService.deleteById(id);
        if(flag){
            return ResponseEntity.ok("删除成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除失败");
    }

}

