package com.geiliwang.web.service;

import com.geiliwang.web.entity.Article;
import com.baomidou.mybatisplus.service.IService;
import com.geiliwang.web.entity.ArticleQuery;
import com.geiliwang.web.entity.MemberVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 文章管理 服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
public interface ArticleService extends IService<Article> {

    PageInfo<Article> queryBy(ArticleQuery query);

    boolean deleteById(Long id);



}
