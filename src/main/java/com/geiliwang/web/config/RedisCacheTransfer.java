package com.geiliwang.web.config;

import org.springframework.data.redis.connection.RedisConnectionFactory;

public class RedisCacheTransfer {

    public void setJedisConnectionFactory(
            RedisConnectionFactory jedisConnectionFactory) {
        RedisClient.setRedisConnectionFactory(jedisConnectionFactory);
    }
}
