package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.*;
import com.geiliwang.web.mapper.MemberMapper;
import com.geiliwang.web.service.MemberService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Service
@Transactional(rollbackFor = Exception.class)
public class MemberServiceImpl extends ServiceImpl<MemberMapper, Member> implements MemberService {
    @Autowired
    private MemberMapper memberMapper;

    @Override
    public boolean register(Member member) {
        try {
            EntityWrapper<Member> ew = new EntityWrapper<>();
            ew.eq("email",member.getEmail());
            List<Member> list = memberMapper.selectList(ew);
            if(CollectionUtils.isEmpty(list)){
                String encryptPasswd = new String(DigestUtils.md5(member.getPasswd().getBytes()));
                member.setPasswd(encryptPasswd);
                member.setRegisterTime(new Date());
                int result = memberMapper.insert(member);
                if(result > 0 ){
                    if(null != member.getInviteId()){
                        Member inviteMember = memberMapper.selectById(member.getInviteId());
                        //todo 增加积分
                    }
                    return true;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public PageInfo<Member> queryBy(MemberQuery query) {
        EntityWrapper<Member> ew = new EntityWrapper<>();
        ew.orderBy("registerTime",false);
        if(StringUtils.isNoneBlank(query.getEmail())){
            ew.like("email",query.getEmail());
        }
        if(StringUtils.isNoneBlank(query.getNickName())){
            ew.like("nickName",query.getNickName());
        }
        PageHelper.startPage(query.getPageNum(),query.getPageSize());
        List<Member> articleList = memberMapper.selectList(ew);
        return new PageInfo<>(articleList);
    }

    @Override
    public List<MemberVO> queryInvites(String email) {
        List<MemberVO> memberVOList = new ArrayList<>();
        if(StringUtils.isNoneBlank(email)) {
            EntityWrapper<Member> ew = new EntityWrapper<>();
            ew.like("email",email);
            List<Member> memberList = memberMapper.selectList(ew);
            if(!CollectionUtils.isEmpty(memberList)){
                ew = new EntityWrapper<>();
                for(Member member : memberList){
                    ew.eq("inviteId",member.getId());
                    List<Member> invites = memberMapper.selectList(ew);
                    if(!CollectionUtils.isEmpty(invites)){
                        memberVOList.addAll(invites.stream().map(m->{
                            MemberVO memberVO = new MemberVO();
                            memberVO.setEmail(m.getEmail());
                            memberVO.setNickName(m.getNickName());
                            memberVO.setRegisterTime(m.getRegisterTime());
                            memberVO.setInviteEmail(member.getEmail());
                            return memberVO;
                        }).collect(Collectors.toList()));
                    }
                }
            }
        }
        return memberVOList;
    }

    @Override
    public int findMemberCount() {
        EntityWrapper<Member> ew = new EntityWrapper<>();
        return memberMapper.selectCount(ew);
    }
}
