package com.geiliwang.web.entity;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by yanjun on 2017/5/13.
 */
public class SmsInfo {


    private String SMSTempateCode;
    private Map<String, String> SMSTemplateParams = new HashMap();
    private String SignName;
    private String phone;
    // public String phone[];

    public String getSignName() {
        return SignName;
    }

    public void setSignName(String signName) {
        SignName = signName;
    }

    public String getSMSTempateCode() {
        return SMSTempateCode;
    }

    public void setSMSTempateCode(String SMSTempateCode) {
        this.SMSTempateCode = SMSTempateCode;
    }

    public Map<String, String> getSMSTemplateParams() {
        return SMSTemplateParams;
    }

    public void setSMSTemplateParams(Map<String, String> SMSTemplateParams) {
        this.SMSTemplateParams = SMSTemplateParams;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


}
