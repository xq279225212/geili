package com.geiliwang.web.service;

import com.geiliwang.web.entity.Platformorder;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 平台提交信息 服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-15
 */
public interface PlatformorderService extends IService<Platformorder> {

    /**
     * 查询会员提交信息
     * @param memberId
     * @return
     */
    List<Platformorder> findByMemberId(Long memberId);

}
