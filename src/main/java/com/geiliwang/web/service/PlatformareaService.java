package com.geiliwang.web.service;

import com.geiliwang.web.entity.Platformarea;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public interface PlatformareaService extends IService<Platformarea> {
    /**
     * 查询未删除状态
     * @return
     */
    List<Platformarea> selectAllNoDel();
}
