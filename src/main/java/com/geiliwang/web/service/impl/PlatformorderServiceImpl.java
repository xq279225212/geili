package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Platformorder;
import com.geiliwang.web.mapper.PlatformorderMapper;
import com.geiliwang.web.service.PlatformorderService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 平台提交信息 服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-15
 */
@Service
public class PlatformorderServiceImpl extends ServiceImpl<PlatformorderMapper, Platformorder> implements PlatformorderService {

    @Autowired
    private PlatformorderMapper platformorderMapper;

    @Override
    public List<Platformorder> findByMemberId(Long memberId) {
        EntityWrapper<Platformorder> ew = new EntityWrapper<>();
        ew.eq("memberId",memberId);
        return platformorderMapper.selectList(ew);
    }
}
