package com.geiliwang.web.enums;

import org.apache.commons.lang3.StringUtils;

/**
 * 文章类别枚举
 */
public enum ArticleTypeEnum {

    INDUSTRY_INFORMATION("1","行业资讯"),
    PLATFORM_SPECIAL("2","平台相关"),
    OTHER_FINANCIAL("3","其它金融"),
    ;

    ArticleTypeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public static String getByCode(String code){
        for(ArticleTypeEnum type:ArticleTypeEnum.values()){
            if(StringUtils.equals(type.getCode(),code)){
                return type.getName();
            }
        }
        return "";
    }

    private String code;

    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
