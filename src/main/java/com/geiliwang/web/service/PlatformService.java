package com.geiliwang.web.service;

import com.geiliwang.web.entity.Platform;
import com.baomidou.mybatisplus.service.IService;
import com.geiliwang.web.entity.PlatformQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public interface PlatformService extends IService<Platform> {

    /**
     * 分页查询
     * @param query
     * @return
     */
    PageInfo<Platform> queryBy(PlatformQuery query);

    /**
     * 查询平台总数量
     * @return
     */
    int findPlatformCount();

    /**
     * 查询置顶平台
     * @return
     */
    List<Platform> findTopPlatform();

}
