package com.geiliwang.web.entity;

import java.util.List;

public class PlatformVO extends Platform {

    private List<Platformscheme> schemeList;

    public List<Platformscheme> getSchemeList() {
        return schemeList;
    }

    public void setSchemeList(List<Platformscheme> schemeList) {
        this.schemeList = schemeList;
    }
}
