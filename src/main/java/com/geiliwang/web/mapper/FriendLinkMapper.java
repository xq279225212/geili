package com.geiliwang.web.mapper;

import com.geiliwang.web.entity.FriendLink;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 友情链接 Mapper 接口
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Mapper
public interface FriendLinkMapper extends BaseMapper<FriendLink> {

}
