package com.geiliwang.web.enums;

/**
 * 投票次数枚举
 */
public enum InvestStyleEnum {

    FIRST_INVEST("1","仅限首投"),
    MANY_INVEST("2","可复投")
    ;

    private String code;

    private String name;

    InvestStyleEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
