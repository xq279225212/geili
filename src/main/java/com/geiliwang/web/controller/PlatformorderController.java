package com.geiliwang.web.controller;


import com.geiliwang.web.entity.Member;
import com.geiliwang.web.entity.Platformorder;
import com.geiliwang.web.service.PlatformorderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>
 * 平台提交信息 前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-15
 */
@Controller
@RequestMapping("/member/platformorder")
public class PlatformorderController {

    @Autowired
    private PlatformorderService platformorderService;

    @RequestMapping("/saveOrder")
    @ResponseBody
    public ResponseEntity<String> saveOrder(Platformorder platformorder, HttpServletRequest request){
        Member member = (Member)request.getSession().getAttribute("member");
        if(null != member){
            platformorder.setMemberId(member.getId());
            boolean flag = platformorderService.insert(platformorder);
            if(flag){
                return ResponseEntity.ok("提交成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("提交失败");
    }

}

