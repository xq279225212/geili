package com.geiliwang.web.mapper;

import com.geiliwang.web.entity.Platformorder;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 平台提交信息 Mapper 接口
 * </p>
 *
 * @author 邓
 * @since 2018-05-15
 */
@Mapper
public interface PlatformorderMapper extends BaseMapper<Platformorder> {

}
