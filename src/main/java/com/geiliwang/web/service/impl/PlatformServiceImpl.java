package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Platform;
import com.geiliwang.web.entity.PlatformQuery;
import com.geiliwang.web.mapper.PlatformMapper;
import com.geiliwang.web.service.PlatformService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Service
public class PlatformServiceImpl extends ServiceImpl<PlatformMapper, Platform> implements PlatformService {

    @Autowired
    private PlatformMapper platformMapper;

    @Override
    public PageInfo<Platform> queryBy(PlatformQuery query) {
        EntityWrapper<Platform> ew = new EntityWrapper<>();
        ew.where("delFlag='0'").orderBy("gmtCreate",false);
        if(StringUtils.isNoneBlank(query.getPlatformName())){
            ew.like("platformName",query.getPlatformName());
        }
        if(StringUtils.isNoneBlank(query.getPlatformName())){
            ew.eq("areaId",query.getAreaId());
        }
        PageHelper.startPage(query.getPageNum(),query.getPageSize());
        List<Platform> articleList = platformMapper.selectList(ew);
        return new PageInfo<>(articleList);
    }

    @Override
    public int findPlatformCount() {
        EntityWrapper<Platform> ew = new EntityWrapper<>();
        ew.eq("delFlag","0");
        return platformMapper.selectCount(ew);
    }

    @Override
    public List<Platform> findTopPlatform() {
        EntityWrapper<Platform> ew = new EntityWrapper<>();
        ew.where("delFlag='0'").eq("isTop",'1');
        return platformMapper.selectList(ew);
    }
}
