package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * 平台提交信息
 * </p>
 *
 * @author 邓
 * @since 2018-05-15
 */
public class Platformorder implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 手机号
     */
    private String telphone;
    /**
     * 真实姓名
     */
    private String realname;
    /**
     * 投资金额
     */
    @TableField("investMoney")
    private String investMoney;
    /**
     * 平台id
     */
    @TableField("platformId")
    private Long platformId;
    /**
     * 计划ID
     */
    @TableField("schemeId")
    private Long schemeId;
    /**
     * 状态
     */
    private String status;
    @TableField("memberId")
    private Long memberId;

    private Date gmtCreate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTelphone() {
        return telphone;
    }

    public void setTelphone(String telphone) {
        this.telphone = telphone;
    }

    public String getRealname() {
        return realname;
    }

    public void setRealname(String realname) {
        this.realname = realname;
    }

    public String getInvestMoney() {
        return investMoney;
    }

    public void setInvestMoney(String investMoney) {
        this.investMoney = investMoney;
    }

    public Long getPlatformId() {
        return platformId;
    }

    public void setPlatformId(Long platformId) {
        this.platformId = platformId;
    }

    public Long getSchemeId() {
        return schemeId;
    }

    public void setSchemeId(Long schemeId) {
        this.schemeId = schemeId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getMemberId() {
        return memberId;
    }

    public void setMemberId(Long memberId) {
        this.memberId = memberId;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    public String toString() {
        return "Platformorder{" +
        ", id=" + id +
        ", telphone=" + telphone +
        ", realname=" + realname +
        ", investMoney=" + investMoney +
        ", platformId=" + platformId +
        ", schemeId=" + schemeId +
        ", status=" + status +
        "}";
    }
}
