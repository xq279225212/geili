package com.geiliwang.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;

@Controller
@RequestMapping("/manage")
public class ManageController {

    @RequestMapping(value = {"","/index"})
    public String index(){
        return "pages/index";
    }

    @RequestMapping("/manageLogout")
    public String manageLogout(HttpServletRequest request){
        request.getSession().removeAttribute("manage");
        return "redirect:/toManageLogin";
    }

    @RequestMapping("/welcome")
    public String welcome(Model model){
        model.addAttribute("nowTime",new Date());
        return "/pages/welcome";
    }
}
