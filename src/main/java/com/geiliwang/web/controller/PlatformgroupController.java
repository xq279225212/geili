package com.geiliwang.web.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Platform;
import com.geiliwang.web.entity.PlatformGroupVO;
import com.geiliwang.web.entity.Platformgroup;
import com.geiliwang.web.service.PlatformService;
import com.geiliwang.web.service.PlatformgroupService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.*;
import java.util.stream.Collectors;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-06
 */
@Controller
@RequestMapping("/manage/platformgroup")
public class PlatformgroupController {

    @Autowired
    private PlatformgroupService platformgroupService;

    @Autowired
    private PlatformService platformService;

    @RequestMapping("/toList")
    public String toList(Model model){
        List<PlatformGroupVO> platformGroupVOList = platformgroupService.findAllGroup();
        model.addAttribute("platformGroupVOList",platformGroupVOList);
        return "/pages/group/group-list";
    }

    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        EntityWrapper<Platform> ew = new EntityWrapper<>();
        ew.where("delFlag='0'");
        List<Platform> platformList = platformService.selectList(ew);
        model.addAttribute("platformList",platformList);
        return "/pages/group/group-add";
    }

    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<String> add(Platformgroup platformgroup){
        boolean flag = platformgroupService.insert(platformgroup);
        if(flag){
            return ResponseEntity.ok("保存成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }

    /**
     * 排序
     * @param sourceId
     * @param destId
     * @return
     */
    @RequestMapping("/groupOrder")
    @ResponseBody
    public ResponseEntity<String> groupOrder(Long sourceId,Long destId){
        Platformgroup sourceGroup = platformgroupService.selectById(sourceId);
        Platformgroup destGroup = platformgroupService.selectById(destId);
        if(null != sourceGroup && null != destGroup){
            sourceGroup.setGroupOrder(destGroup.getGroupOrder()+1);
            boolean flag = platformgroupService.updateById(sourceGroup);
            if(flag){
                return ResponseEntity.ok("保存成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseEntity<String> delete(@RequestParam("id")Long id){
        Platformgroup platformgroup = platformgroupService.selectById(id);
        if(null != platformgroup) {
            platformgroup.setDelFlag("1");
            boolean flag = platformgroupService.updateById(platformgroup);
            if (flag) {
                return ResponseEntity.ok("删除成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除失败");
    }

    /**
     *
     * @return
     */
    @RequestMapping("/toUpdate")
    public String toUpdate(Long id,Model model){
        Platformgroup platformgroup = platformgroupService.selectById(id);
        EntityWrapper<Platform> ew = new EntityWrapper<>();
        ew.where("delFlag='0'");
        List<Platform> platformList = platformService.selectList(ew);
        if(null != platformgroup){
            if(StringUtils.isNoneBlank(platformgroup.getPlatformIds())) {
                List<String> arr = Arrays.asList(StringUtils.split(platformgroup.getPlatformIds(),","));
                final List<Long> platformIds =  arr.stream().map(str->Long.valueOf(str)).collect(Collectors.toList());
                //过滤出选中的平台，还要排序
                List<Platform> checkedPlatformList = new ArrayList<>();
                if(StringUtils.isNoneBlank(platformgroup.getPlatformIds())){
                    //checkedPlatformList = platformList.stream().filter(platform -> platformIds.contains(platform.getId())).collect(Collectors.toList());
                    Map<Long,Platform> platformMap = platformList.stream().collect(Collectors.toMap(Platform::getId, platform->platform));
                    checkedPlatformList = platformIds.stream().map(platformId->platformMap.get(platformId)).collect(Collectors.toList());
                }
                model.addAttribute("platformIds",platformIds);
                model.addAttribute("checkedPlatformList",checkedPlatformList);
            }
            model.addAttribute("platformList",platformList);
            model.addAttribute("platformgroup",platformgroup);
            return  "/pages/group/group-edit";
        }
        return "404";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String updateArticle(Platformgroup platformgroup){
        platformgroupService.updateById(platformgroup);
        return "";
    }
}

