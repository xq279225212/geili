package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 友情链接
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
public class FriendLink implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 网站名称
     */
    @TableField("websiteName")
    private String websiteName;
    /**
     * 网站地址
     */
    @TableField("websiteUrl")
    private String websiteUrl;
    /**
     * 0:启用 1：禁用
     */
    @TableField("isUsed")
    private String isUsed;
    @TableField("gmtCreate")
    private Date gmtCreate;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWebsiteName() {
        return websiteName;
    }

    public void setWebsiteName(String websiteName) {
        this.websiteName = websiteName;
    }

    public String getWebsiteUrl() {
        return websiteUrl;
    }

    public void setWebsiteUrl(String websiteUrl) {
        this.websiteUrl = websiteUrl;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    @Override
    public String toString() {
        return "FriendLink{" +
        ", id=" + id +
        ", websiteName=" + websiteName +
        ", websiteUrl=" + websiteUrl +
        ", isUsed=" + isUsed +
        ", gmtCreate=" + gmtCreate +
        "}";
    }
}
