package com.geiliwang.web.service;

import com.geiliwang.web.entity.Banner;
import com.baomidou.mybatisplus.service.IService;
import com.geiliwang.web.entity.BannerQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * banner管理 服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
public interface BannerService extends IService<Banner> {

    PageInfo<Banner> queryBy(BannerQuery query);

    List<Banner> findUsedBanners();

}
