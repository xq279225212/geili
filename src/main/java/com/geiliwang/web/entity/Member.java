package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public class Member implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 邮箱
     */
    @NotNull
    private String email;
    /**
     * 昵称
     */
    @TableField("nickName")
    private String nickName;
    /**
     * 密码
     */
    @NotNull
    private String passwd;
    /**
     * 积分
     */
    private Integer integral;
    /**
     * 注册时间
     */
    @TableField("registerTime")
    private Date registerTime;
    /**
     * 性别
     */
    private String sex;
    /**
     * QQ
     */
    private String qq;
    /**
     * 支付宝账号
     */
    @TableField("alipayAccount")
    private String alipayAccount;
    /**
     * 支付宝姓名
     */
    @TableField("alipayName")
    private String alipayName;
    /**
     * 邀请人id
     */
    @TableField("inviteId")
    private Long inviteId;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getPasswd() {
        return passwd;
    }

    public void setPasswd(String passwd) {
        this.passwd = passwd;
    }

    public Integer getIntegral() {
        return integral;
    }

    public void setIntegral(Integer integral) {
        this.integral = integral;
    }

    public Date getRegisterTime() {
        return registerTime;
    }

    public void setRegisterTime(Date registerTime) {
        this.registerTime = registerTime;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getQq() {
        return qq;
    }

    public void setQq(String qq) {
        this.qq = qq;
    }

    public String getAlipayAccount() {
        return alipayAccount;
    }

    public void setAlipayAccount(String alipayAccount) {
        this.alipayAccount = alipayAccount;
    }

    public String getAlipayName() {
        return alipayName;
    }

    public void setAlipayName(String alipayName) {
        this.alipayName = alipayName;
    }

    public Long getInviteId() {
        return inviteId;
    }

    public void setInviteId(Long inviteId) {
        this.inviteId = inviteId;
    }

    @Override
    public String toString() {
        return "Member{" +
        ", id=" + id +
        ", email=" + email +
        ", nickName=" + nickName +
        ", passwd=" + passwd +
        ", integral=" + integral +
        ", registerTime=" + registerTime +
        ", sex=" + sex +
        ", qq=" + qq +
        ", alipayAccount=" + alipayAccount +
        ", alipayName=" + alipayName +
        ", inviteId=" + inviteId +
        "}";
    }
}
