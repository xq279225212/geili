package com.geiliwang.web.entity;

import java.util.ArrayList;
import java.util.List;

public class PlatformGroupVO extends Platformgroup {

    private List<Platform> platformList = new ArrayList<>();

    public List<Platform> getPlatformList() {
        return platformList;
    }

    public void setPlatformList(List<Platform> platformList) {
        this.platformList = platformList;
    }
}
