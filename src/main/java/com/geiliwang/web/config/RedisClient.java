package com.geiliwang.web.config;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.serializer.JdkSerializationRedisSerializer;
import org.springframework.stereotype.Service;

import java.util.HashMap;

@Configuration
@Service
public class RedisClient {

    private static RedisConnectionFactory redisConnectionFactory;
    //    private static RedisConnection conn;
    private static JdkSerializationRedisSerializer jdkSerializer = new JdkSerializationRedisSerializer();
    private static final Logger logger = Logger.getLogger(RedisClient.class);

    private static RedisConnection getConnection() throws IllegalArgumentException {
        if (redisConnectionFactory != null) {
//            RedisConnection conn = ;
            return redisConnectionFactory.getConnection();
        } else {
            throw new IllegalArgumentException("redisConnectionFactory为空!");
        }
    }

    public static void setKeyValue(String key, Object value) {
        //ExpireTime
        RedisConnection conn = null;
        try {
            conn = getConnection();
            if (key == null)
                return;
            conn.set(key.getBytes(), jdkSerializer.serialize(value));
        } finally {
            //返还到连接池
            if (conn != null)
                conn.close();
        }
    }

    public static void setKeyValue(String key, Object value, long seconds) {
        //ExpireTime\
        RedisConnection conn = null;
        try {
            conn = getConnection();
            if (key == null)
                return;
            conn.set(key.getBytes(), jdkSerializer.serialize(value));
            conn.expire(key.getBytes(), seconds);
        } finally {
            //返还到连接池
            if (conn != null)
                conn.close();
        }
    }

    public static Object getKeyValue(String key) throws IllegalArgumentException {
        RedisConnection conn = null;
        try {
            if (key != null) {
                //集群 RedisClusterConnection conn = redisConnectionFactory.getConnection();
                conn = getConnection();
                byte[] bs = conn.get(key.getBytes());
                return jdkSerializer.deserialize(bs);
            } else {
                throw new IllegalArgumentException("key为空!");
            }
        } finally {
            //返还到连接池
            if (conn != null && !conn.isClosed())
                conn.close();
        }
    }

    public static void setHashMap(String key, HashMap map) throws IllegalArgumentException {
        RedisConnection conn = null;
        try {
            conn = getConnection();
            if (null != key && null != map) {
                conn.set(key.getBytes(), jdkSerializer.serialize(map));
            } else {
                throw new IllegalArgumentException("key or value为空!");
            }
        } finally {
            //返还到连接池
            if (conn != null)
                conn.close();
        }
    }

    public static void setHashMap(String key, HashMap map, long seconds) throws IllegalArgumentException {
        RedisConnection conn = null;
        try {
            conn = getConnection();
            if (null != key && null != map) {
                conn.set(key.getBytes(), jdkSerializer.serialize(map));
                conn.expire(key.getBytes(), seconds);
            } else {
                throw new IllegalArgumentException("key or value为空!");
            }
        } finally {
            //返还到连接池
            if (conn != null)
                conn.close();
        }
    }

    public static HashMap<String, String> getHashMap(String key) throws IllegalArgumentException {
        RedisConnection conn = null;
        try {
            if (key != null) {
                //集群 RedisClusterConnection conn = redisConnectionFactory.getConnection();
                conn = getConnection();
                byte[] bs = conn.get(key.getBytes());
                return (HashMap<String, String>) jdkSerializer.deserialize(bs);
            } else {
                throw new IllegalArgumentException("key为空!");
            }
        } finally {
            //返还到连接池
            if (conn != null)
                conn.close();
        }
    }

    public static void clean() {
        RedisConnection conn = null;
        try {
            conn = getConnection();
            conn.flushAll();
        } finally {
            //返还到连接池
            if (conn != null)
                conn.close();
        }
    }


    public static RedisConnectionFactory setRedisConnectionFactory(RedisConnectionFactory redisConnectionFactory) {
        RedisClient.redisConnectionFactory = redisConnectionFactory;
        return redisConnectionFactory;
    }


}