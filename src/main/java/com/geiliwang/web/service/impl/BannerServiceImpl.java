package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Banner;
import com.geiliwang.web.entity.BannerQuery;
import com.geiliwang.web.mapper.BannerMapper;
import com.geiliwang.web.service.BannerService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * banner管理 服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Service
public class BannerServiceImpl extends ServiceImpl<BannerMapper, Banner> implements BannerService {

    @Autowired
    private BannerMapper bannerMapper;

    @Override
    public PageInfo<Banner> queryBy(BannerQuery query) {
        EntityWrapper<Banner> ew = new EntityWrapper<>();
        ew.orderBy("gmtCreate",false);
        PageHelper.startPage(query.getPageNum(),query.getPageSize());
        List<Banner> bannerList = bannerMapper.selectList(ew);
        return new PageInfo<>(bannerList);
    }

    @Override
    public List<Banner> findUsedBanners() {
        EntityWrapper<Banner> ew = new EntityWrapper<>();
        ew.eq("isUsed","0").orderBy("gmtCreate",false);
        return bannerMapper.selectList(ew);
    }

}
