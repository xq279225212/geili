package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import org.apache.commons.lang3.StringUtils;

import java.io.Serializable;

/**
 * <p>
 * banner管理
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
public class Banner implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 图片路径
     */
    @TableField("imgPath")
    private String imgPath;
    /**
     * 跳转url
     */
    @TableField("directUrl")
    private String directUrl;
    /**
     * 0：启用 1：禁用
     */
    @TableField("isUsed")
    private String isUsed;
    @TableField("gmtCreate")
    private Date gmtCreate;
    /**
     * banner类型
     */
    private String type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getDirectUrl() {
        return directUrl;
    }

    public void setDirectUrl(String directUrl) {
        if(!StringUtils.containsIgnoreCase(directUrl,"http://") && !StringUtils.containsIgnoreCase(directUrl,"https://")){
            directUrl = "http://"+directUrl;
        }
        this.directUrl = directUrl;
    }

    public String getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(String isUsed) {
        this.isUsed = isUsed;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Banner{" +
        ", id=" + id +
        ", imgPath=" + imgPath +
        ", directUrl=" + directUrl +
        ", isUsed=" + isUsed +
        ", gmtCreate=" + gmtCreate +
        "}";
    }
}
