package com.geiliwang.web.service;

import com.geiliwang.web.entity.ArticleQuery;
import com.geiliwang.web.entity.Member;
import com.baomidou.mybatisplus.service.IService;
import com.geiliwang.web.entity.MemberQuery;
import com.geiliwang.web.entity.MemberVO;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public interface MemberService extends IService<Member> {

    /**
     * 注册
     * @param member
     * @return
     */
    boolean register(Member member);

    /**
     * 分页查询
     * @param query
     * @return
     */
    PageInfo<Member> queryBy(MemberQuery query);

    /**
     * 查询邀请记录
     * @param email
     * @return
     */
    List<MemberVO> queryInvites(String email);

    /**
     * 会员数量
     * @return
     */
    int findMemberCount();
}
