package com.geiliwang.web.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.*;
import com.geiliwang.web.enums.InvestStyleEnum;
import com.geiliwang.web.enums.PlatformAttrEnum;
import com.geiliwang.web.service.PlatformService;
import com.geiliwang.web.service.PlatformareaService;
import com.geiliwang.web.service.PlatformschemeService;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Controller
@RequestMapping("/manage/platform")
public class PlatformController {
    @Autowired
    private PlatformService platformService;
    @Autowired
    private PlatformschemeService platformschemeService;
    @Autowired
    private PlatformareaService platformareaService;

    @RequestMapping("/toList")
    public String toList(PlatformQuery query,Model model){
        PageInfo<Platform> platformPageInfo = platformService.queryBy(query);
        model.addAttribute("platformPageInfo",platformPageInfo);
        return "/pages/platform/platform-list";
    }

    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        //查询所有有效平台地区
        List<Platformarea> platformareaList = platformareaService.selectAllNoDel();
        model.addAttribute("platformareaList",platformareaList);
        model.addAttribute("attrs", PlatformAttrEnum.values());
        model.addAttribute("invests",InvestStyleEnum.values());
        return "/pages/platform/platform-add";
    }

    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<String> add(Platform platform){
        boolean flag = platformService.insert(platform);
        if(flag){
            return ResponseEntity.ok("保存成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }
    /**
     * 上传图片
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public Map<String,String> uploadFile(MultipartFile file, HttpServletRequest request) {
        Map result = new HashMap();
        try {
            //上传文件路径
            String uploadPath = request.getServletContext().getRealPath("/upload/platform/");
            //上传文件名
            String filename = file.getOriginalFilename();
            filename = System.currentTimeMillis()+filename;
            File filepath = new File(uploadPath,filename);
            //判断路径是否存在，如果不存在就创建一个
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中

            file.transferTo(new File(uploadPath + File.separator + filename));
            result.put("imgPath","/upload/platform/"+filename);
            System.out.println(file);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("imgPath","");
        }
        return result;
    }

    @RequestMapping("/detail")
    public String detail(@RequestParam("id")Long id,Model model){
        Platform platform = platformService.selectById(id);
        Platformarea platformarea = platformareaService.selectById(platform.getAreaId());
        model.addAttribute("platform",platform);
        model.addAttribute("platformarea",platformarea);
        model.addAttribute("attrs", PlatformAttrEnum.values());
        model.addAttribute("invests",InvestStyleEnum.values());
        return "pages/platform/platform-detail";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseEntity<String> delete(@RequestParam("id")Long id){
        Platform platform = platformService.selectById(id);
        if(null != platform) {
            platform.setDelFlag("1");
            boolean flag = platformService.updateById(platform);
            if (flag) {
                return ResponseEntity.ok("删除成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除失败");
    }

    @RequestMapping("/toUpdate")
    public String toUpdate(@RequestParam("id")Long id, Model model){
        Platform platform = platformService.selectById(id);
        Platformarea platformarea = platformareaService.selectById(platform.getAreaId());
        List<Platformarea> platformareaList = platformareaService.selectAllNoDel();
        model.addAttribute("platform",platform);
        model.addAttribute("platformarea",platformarea);
        model.addAttribute("platformareaList",platformareaList);
        model.addAttribute("attrs", PlatformAttrEnum.values());
        model.addAttribute("invests",InvestStyleEnum.values());
        return "/pages/platform/platform-edit";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String updateArticle(Platform platform){
        platformService.updateById(platform);
        return "";
    }

    /**
     * 平台置顶
     * @param id
     * @return
     */
    @RequestMapping("/top")
    @ResponseBody
    public String top(Long id){
        Platform platform = platformService.selectById(id);
        if(null != platform){
            if(StringUtils.equals("1",platform.getIsTop())){
                platform.setIsTop("0");
            }else{
                platform.setIsTop("1");
            }
            boolean flag = platformService.updateById(platform);
            if(flag){
                return "置顶成功";
            }
        }
        return "置顶失败";
    }
}

