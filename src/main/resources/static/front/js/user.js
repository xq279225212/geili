
$(document).ready(function(){
    $('.MJF_tab_info_v3 a:first').tab('show');
    $('.MJF_tab_info_v3 a').click(function (e) {
        e.preventDefault();
        $(this).tab('show');
    })

});
var get_data = {
    data_url: '/user-ajax_get_data.html'
};
get_data.create_data = function(id, data)
{
    var str = '', _status;
    switch(id) {
        case 'info_1':
            for(var i in data) {
                if(data[i].status == 1) _status = '<span class="STYLE2">确认中</span>';
                else if(data[i].status == 2) _status = '<span class="STYLE36">有效</span>';
                else _status = '<span class="STYLE38">无效</span>';
                str +='<tr><td bgcolor="#fcfcfc" align="center"><span class="STYLE2"><a href="/job-info_'+data[i].job_id+'.html" target="_blank">'+data[i].title+'</a></span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE2">'+data[i].info+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+data[i].done_time+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+data[i].score+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+data[i].magic+'</span></td> <td bgcolor="#fcfcfc" align="center">'+_status+'</td> </tr>';
            }
            $('#data_1').html(str);
            break;
        case 'info_2':
            for(var i in data) {
                //var _sxf = data[i].score < 1000000 ? 1 : 0;
                if(data[i].status == 1) _status = '<span class="STYLE2">等待支付</span>';
                else _status = '<span class="STYLE36">已支付</span>';
                str += ' <tr> <td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+data[i].add_time+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE2">'+data[i].score+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+(parseInt(data[i].money)+parseInt(data[i].coupons))+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+data[i].coupons+'</span></td> <td bgcolor="#fcfcfc" align="center">'+_status+'</td> </tr>';
            }
            $('#data_2').html(str);
            break;
        case 'info_3':
            for(var i in data) {
                str += '<tr> <td bgcolor="#fcfcfc" align="center"><span class="STYLE2">'+data[i].nickname+'</span></td><td bgcolor="#fcfcfc" align="center"><span class="STYLE2">'+data[i].add_time+'</span></td> <td bgcolor="#fcfcfc" align="center"><span class="STYLE34">'+data[i].add_score+'</span></td> </tr>';
            }
            $('#data_3').html(str);
            $('#data_3_friends_num').html(user_page.all_num.info_3);
            break;
    }
};

get_data.load_data = function(group, page) {
    $.get('/user-ajax_get_data.html',
        {
            id: group.replace("info_", ""),
            p: page
        }, function(res){
            get_data.create_data(group, res);
            user_page.allow_load_flag = true;
        }, 'json');
}
var user_page = {
    allow_load_flag:true,
    all_num:{},
    all_page: {},
    current_page: {}
};

user_page.init = function(data)
{
    $('#data_3_friends_money').html(data['info_3']['all_money']);
    for(var i in data)
    {
        this.all_page[i] = data[i].all_page;
        this.all_num[i] = data[i].all_num;
        this.current_page[i] = 1;

        if(data[i].first_data)
            get_data.create_data(i, data[i].first_data);
        if(data[i].all_page)
            this.page_layout(i);
    }
};
user_page.page_layout = function(view){
    if(!view) return false;
    var key = view.replace('info_', '');

    var prev,next,curr, active,disable_p,disable_n,
        all=this.all_page[view],
        for_s=1,
        for_e=all;
    curr = this.current_page[view];
    next = curr == all ? curr : curr - 0 + 1;
    prev = curr == 1 ? 1 : curr -1;
    if(all > 5 ) {
        if(curr-2 >= 1) {
            for_s = curr - 2;
            for_e = curr - 0 + 2;
            if(curr-0+2 >= all) {
                for_s = all - 4;
                for_e = all;
            }
        }
        else
            for_e = 5;
    }
    disable_p = curr == 1 ? ' class="disabled" ' : '';
    disable_n = curr == all ? ' class="disabled" ' : '';
    var str = '<ul class="pagination pagination-sm"><li'+disable_p+'><a data-group="'+view+'" data-page='+prev+' href="###">&laquo;</a></li>';
    for(var j=for_s; j<=for_e; j++) {
        if(j == curr) active = ' class="active" ';
        else active = '';
        str += '<li'+active+'><a data-group="'+view+'" data-page="'+j+'" href="###">'+j+'</a></li>';
    }
    str += '<li'+disable_n+'><a data-group="'+view+'" data-page="'+next+'" href="###">&raquo;</a></li></ul>';
    $('#page_'+key).html(str);
    this.add_event(view);
};
user_page.add_event = function(view) {
    $('#'+view+' .pagination-sm a').click(function(){
        var p_class = $(this).parent().attr('class');
        if(p_class == 'disabled' || p_class == 'active') return false;
        if(!user_page.allow_load_flag) return false;
        user_page.allow_load_flag = false;
        var group = $(this).attr('data-group'),
            page = $(this).attr('data-page');
        user_page.current_page[group] = page;
        user_page.page_layout(group);
        get_data.load_data(group, page);
    });
};


$("#user_excharge_btn").click(function(){
    $(".MJF_excharge_show_div").show();
    $(".MJF_excharge_show_div").animate({
        height:"450px"
    });
    $('#alipay_a').tooltip('hide');
});

$('#close_user_excharge_btn').click(function(){
    close_user_excharge_div_func();
});

var close_user_excharge_div_func = function(show_tip){
    $(".MJF_excharge_show_div").animate({
        height:"0px"
    },function(){
        $('.MJF_excharge_show_div').hide();
        $('#submit_msg').hide();
        $('#submit_clock').html("");
        if(show_tip)
            $('#alipay_a').tooltip('show');
    });
};

var coupons_money = 0, real_money=0, coupons_flag=0;
$('.coupons_a').click(function(){
    if(real_money == 0) { create_warning("请输入正确的提现积分"); return false; }

    coupons_flag = $(this).attr('data-flag');
    coupons_money = coupons_flag.split('-')[0];

    if((coupons_money == 10 && real_money < 1200) ||
        (coupons_money == 20 && real_money < 2300) ||
        (coupons_money == 50 && real_money < 5000) ||
        (coupons_money == 100 && real_money < 9000)
    ) {
        coupons_money = 0;
        coupons_flag = 0;
        create_warning("本次提现金额未达到使用现金券要求");
        return false;
    }

    $('.coupons_a a').removeClass('active');
    $(this).find('a').addClass('active')
    if(real_money > 0)
        $('#_real_money_').html(parseInt(coupons_money) + parseInt(real_money));
})

$('.MJF_edit_profile').click(function(){
    var _objs = $('.MJF_INFO_TAG');
    _objs.each(function(){
        var _id = 3;
        $('.item').each(function(){
            var _item_id = $(this).attr('data-num');
            if(_id == $(this).attr('data-num')) {
                $(this).css('display', 'block');
                $(_objs[_item_id]).addClass('nav-tabs-sel');
            } else {
                $(this).css('display', 'none');
                $(_objs[_item_id]).removeClass('nav-tabs-sel');
            }
        });
    })
    close_user_excharge_div_func(true);
    $('#alipay_input').focus();
});

function _reset() {
    $('#_real_money_').html("0");
    coupons_money = 0;
    $('.coupons_a a').removeClass('active');
    /*$('#_poundage_').html("0");*/
}

$('#_excharge_money_input_').keyup(function(){
    var preg = /(^[1-9]\d*$)/,
        val=$(this).val(),
        res = 0;
    $('.coupons_a a').removeClass('active');
    coupons_money = 0;
    coupons_flag = 0;

    if(!preg.test(val)) {
        $('#_excharge_money_span_').html("格式错误");
        _reset();
    } else {
        if(val % 100000 != 0) {
            $('#_excharge_money_span_').html("输入的积分必须是100000的整数倍");
            _reset();
        } else if(val > score ) {
            $('#_excharge_money_span_').html("超出现有积分");
            _reset();
        } else if(val < 100000 ) {
            $('#_excharge_money_span_').html("最低提现积分100000");
            _reset();
        } else {
            var _l = 0;
            val = val / 10000;
            $('#_excharge_money_span_').html('积分,&nbsp;约等于<span class="STYLE47"> '+val+' </span>元人民币');
            /*
            if(val >= 100)
                $('#_poundage_').html("0");
            else {
                val = val - 1;
                $('#_poundage_').html("1");
            }
            */
            real_money = val;
            if(coupons_money > 0)
                val = parseInt(val) + parseInt(coupons_money);
            $('#_real_money_').html(val);
        }
    }
});

$('#_excharge_money_btn_').click(function() {
    var val = $('#_excharge_money_input_').val();
    var coupons = 0;
    if(coupons_flag)
        coupons = coupons_flag.split('-')[1];

    if(val % 100000 != 0) {
        create_warning('提现积分必须是100000的整数倍');
        return false;
    }
    if(val < 100000) {
        create_warning('提现积分必须大于100000');
        return false;
    }
    if(val > score) {
        create_warning('超出现有积分');
        return false;
    }
    if(!confirm("申请提现后,将不可取消,确认提现吗?")) {
        return false;
    }
    $.post('/user-ajax_excharge_do.html',{excharge_score:val,coupons:coupons},function(res){
        $('#submit_msg').html(res.msg);
        $('#submit_msg').show();
        if(res.status < 0)
        {
            $('#submit_msg').addClass('alert-danger');
            return false;
        }

        $('.user_score').html(res.score);
        $('#_rmb_').html(res.score / 10000);
        $('#_excharge_money_input_').val('');
        $('.STYLE47').html("0");
        $('#submit_clock').html('&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3秒后关闭!');
        get_data.load_data("info_2", 1);
        setTimeout(function(){
            close_user_excharge_div_func();
        }, 3000);
    }, 'json');
    return true;
});

var create_warning_v = '';
var create_warning = function(msg) {
    $('#warning_span').remove();
    clearTimeout(create_warning_v);
    var str = '<span id="warning_span" class="alert alert-danger" role="alert">'+msg+'</span>';
    $('.submit_td').append(str);
    create_warning_v = setTimeout(function(){
        $('#warning_span').remove();
    }, 2000);
}
