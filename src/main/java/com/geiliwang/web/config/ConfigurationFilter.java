package com.geiliwang.web.config;

import com.geiliwang.web.entity.Member;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Configuration
public class ConfigurationFilter {

//    @Bean
    public FilterRegistrationBean frontFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new FrontFilter());//添加过滤器
        registration.addUrlPatterns("/member/*");//设置过滤路径，/*所有路径
        registration.setName("FrontFilter");//设置优先级
        registration.setOrder(1);//设置优先级
        return registration;
    }

    @Bean
    public FilterRegistrationBean pagesFilterRegistration() {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new PagesFilter());//添加过滤器
        registration.addUrlPatterns("/manage/*");//设置过滤路径，/*所有路径
        registration.setName("PageFilter");//设置优先级
        registration.setOrder(2);//设置优先级
        return registration;
    }

    /**
     * 前台过滤
     */
    public class FrontFilter implements Filter {
        @Override
        public void destroy() {
        }

        @Override
        public void doFilter(ServletRequest srequest, ServletResponse sresponse, FilterChain
                filterChain)
                throws IOException, ServletException {
            HttpServletRequest request = (HttpServletRequest) srequest;
            //打印请求Url
            System.out.println("this is FrontFilter,url :" + request.getRequestURI());
            Member member = (Member) request.getSession().getAttribute("member");
            if(null != member) {
                filterChain.doFilter(srequest, sresponse);
            }else{
                request.getRequestDispatcher("/toLogin").forward(request,sresponse);
            }
        }

        @Override
        public void init(FilterConfig arg0) throws ServletException {
        }
    }

    /**
     * 后台过滤
     */
    public class PagesFilter implements Filter {
        @Override
        public void destroy() {
        }

        @Override
        public void doFilter(ServletRequest srequest, ServletResponse sresponse, FilterChain
                filterChain)
                throws IOException, ServletException {
            HttpServletRequest request = (HttpServletRequest) srequest;
            //打印请求Url
            System.out.println("this is PagesFilter,url :" + request.getRequestURI());
            Object manage = request.getSession().getAttribute("manage");
            if(null != manage) {
                filterChain.doFilter(srequest, sresponse);
            }else{
                request.getRequestDispatcher("/toManageLogin").forward(request,sresponse);
            }
        }

        @Override
        public void init(FilterConfig arg0) throws ServletException {
        }
    }

}
