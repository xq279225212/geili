package com.geiliwang.web.service.impl;

import com.geiliwang.web.entity.Platformscheme;
import com.geiliwang.web.mapper.PlatformschemeMapper;
import com.geiliwang.web.service.PlatformschemeService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Service
public class PlatformschemeServiceImpl extends ServiceImpl<PlatformschemeMapper, Platformscheme> implements PlatformschemeService {

}
