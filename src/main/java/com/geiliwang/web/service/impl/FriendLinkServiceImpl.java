package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.FriendLink;
import com.geiliwang.web.entity.FriendQuery;
import com.geiliwang.web.mapper.FriendLinkMapper;
import com.geiliwang.web.service.FriendLinkService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 * 友情链接 服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Service
public class FriendLinkServiceImpl extends ServiceImpl<FriendLinkMapper, FriendLink> implements FriendLinkService {

    @Autowired
    private FriendLinkMapper friendLinkMapper;

    @Override
    public PageInfo<FriendLink> queryBy(FriendQuery query) {
        EntityWrapper<FriendLink> ew = new EntityWrapper<>();
        ew.orderBy("gmtCreate",false);
        PageHelper.startPage(query.getPageNum(),query.getPageSize());
        List<FriendLink> articleList = friendLinkMapper.selectList(ew);
        return new PageInfo<>(articleList);
    }

    @Override
    public List<FriendLink> findUsedLink() {
        EntityWrapper<FriendLink> ew = new EntityWrapper<>();
        ew.orderBy("gmtCreate",false).eq("isUsed","0");
        return friendLinkMapper.selectList(ew);
    }
}
