package com.geiliwang.web.controller;


import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Platformarea;
import com.geiliwang.web.service.PlatformareaService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Controller
@RequestMapping("/manage/platformarea")
public class PlatformareaController {

    @Autowired
    private PlatformareaService platformareaService;

    @RequestMapping("/toList")
    public String toList(Model model){
        List<Platformarea> platformareaList = platformareaService.selectList(new EntityWrapper<>());
        model.addAttribute("platformareaList",platformareaList);
        return "/pages/area/area-list";
    }
    @RequestMapping("/update")
    @ResponseBody
    public ResponseEntity<String> update(Long id){
        Platformarea area = platformareaService.selectById(id);
        if(null != area){
            if(StringUtils.equals("0",area.getDelFlag())){
                area.setDelFlag("1");
            }else if(StringUtils.equals("1",area.getDelFlag())){
                area.setDelFlag("0");
            }
            boolean flag = platformareaService.updateById(area);
            if(flag){
                return ResponseEntity.ok("修改成功");
            }
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("修改失败");
    }

    @RequestMapping("/toAdd")
    public String toAdd(){
        return "/pages/area/area-add";
    }

    @RequestMapping("/add")
    @ResponseBody
    public ResponseEntity<String> add(Platformarea area){
        boolean flag = platformareaService.insert(area);
        if(flag){
            return ResponseEntity.ok("保存成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("保存失败");
    }
    @RequestMapping("/delete")
    @ResponseBody
    public ResponseEntity<String> delete(@RequestParam("id")Integer id){
        boolean flag = platformareaService.deleteById(id);
        if(flag){
            return ResponseEntity.ok("删除成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除失败");
    }

}

