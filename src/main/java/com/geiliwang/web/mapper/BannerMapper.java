package com.geiliwang.web.mapper;

import com.geiliwang.web.entity.Banner;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * banner管理 Mapper 接口
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Mapper
public interface BannerMapper extends BaseMapper<Banner> {

}
