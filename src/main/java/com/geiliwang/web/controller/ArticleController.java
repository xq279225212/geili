package com.geiliwang.web.controller;


import com.geiliwang.web.entity.Article;
import com.geiliwang.web.entity.ArticleQuery;
import com.geiliwang.web.enums.ArticleTypeEnum;
import com.geiliwang.web.service.ArticleService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 文章管理 前端控制器
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Controller
@RequestMapping("/manage/article")
public class ArticleController {
    @Autowired
    private ArticleService articleService;

    /**
     * 文章列表
     * @return
     */
    @RequestMapping("/articleList")
    public String list(ArticleQuery query,Model model){
        PageInfo<Article> articlePage = articleService.queryBy(query);
        articlePage.getList().forEach(article -> {
            article.setType(ArticleTypeEnum.getByCode(article.getType()));
        });
        model.addAttribute("articlePage",articlePage);
        model.addAttribute("query",query);
        model.addAttribute("articleTypes", ArticleTypeEnum.values());
        return "pages/article/article-list";
    }

    @RequestMapping("/detail")
    public String detail(@RequestParam("id")Long id,Model model){
        Article article = articleService.selectById(id);
        if(null != article){
            article.setType(ArticleTypeEnum.getByCode(article.getType()));
        }
        model.addAttribute("article",article);
        return "pages/article/article-detail";
    }

    @RequestMapping("/delete")
    @ResponseBody
    public ResponseEntity<String> delete(@RequestParam("id")Long id){
        boolean flag = articleService.deleteById(id);
        if(flag){
            return ResponseEntity.ok("删除成功");
        }
        return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("删除失败");
    }

    @RequestMapping("/toAdd")
    public String toAdd(Model model){
        model.addAttribute("articleTypes", ArticleTypeEnum.values());
        return "/pages/article/article-add";
    }

    @RequestMapping("/add")
    public String addArticle(Article article){
        articleService.insert(article);
        return "redirect:/manage/article/articleList";
    }

    @RequestMapping("/toUpdate")
    public String toUpdate(@RequestParam("id")Long id, Model model){
        Article article = articleService.selectById(id);
        model.addAttribute("articleTypes", ArticleTypeEnum.values());
        model.addAttribute("article",article);
        return "/pages/article/article-edit";
    }

    @RequestMapping("/update")
    @ResponseBody
    public String updateArticle(Article article){
        articleService.updateById(article);
        return "";
    }

    /**
     * 上传图片
     * @return
     */
    @RequestMapping("/upload")
    @ResponseBody
    public Map<String,String> uploadFile(MultipartFile file, HttpServletRequest request) {
        Map result = new HashMap();
        try {
            //上传文件路径
            String uploadPath = request.getServletContext().getRealPath("/upload/article/");
            //上传文件名
            String filename = file.getOriginalFilename();
            filename = System.currentTimeMillis()+filename;
            File filepath = new File(uploadPath,filename);
            //判断路径是否存在，如果不存在就创建一个
            if (!filepath.getParentFile().exists()) {
                filepath.getParentFile().mkdirs();
            }
            //将上传文件保存到一个目标文件当中

            file.transferTo(new File(uploadPath + File.separator + filename));
            result.put("imgPath","/upload/article/"+filename);
            System.out.println(file);
        } catch (Exception e) {
            e.printStackTrace();
            result.put("imgPath","");
        }
        return result;
    }

}

