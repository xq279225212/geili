package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 文章管理
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 图片路径
     */
    @TableField("imgPath")
    private String imgPath;
    /**
     * 文章内容
     */
    private String content;
    /**
     * 文章类型
     */
    private String type;
    /**
     * 文章来源
     */
    private String source;
    /**
     * 1：置顶 0：正常
     */
    @TableField("isTop")
    private String isTop;
    /**
     * 创建时间
     */
    @TableField("gmtCreate")
    private Date gmtCreate;
    /**
     * 0:正常 1：删除
     */
    @TableField("delFlag")
    private String delFlag;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImgPath() {
        return imgPath;
    }

    public void setImgPath(String imgPath) {
        this.imgPath = imgPath;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getIsTop() {
        return isTop;
    }

    public void setIsTop(String isTop) {
        this.isTop = isTop;
    }

    public Date getGmtCreate() {
        return gmtCreate;
    }

    public void setGmtCreate(Date gmtCreate) {
        this.gmtCreate = gmtCreate;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Article{" +
        ", id=" + id +
        ", title=" + title +
        ", imgPath=" + imgPath +
        ", content=" + content +
        ", type=" + type +
        ", source=" + source +
        ", isTop=" + isTop +
        ", gmtCreate=" + gmtCreate +
        ", delFlag=" + delFlag +
        "}";
    }
}
