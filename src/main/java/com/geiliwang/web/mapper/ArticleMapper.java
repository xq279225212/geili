package com.geiliwang.web.mapper;

import com.geiliwang.web.entity.Article;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import com.geiliwang.web.entity.ArticleQuery;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 * 文章管理 Mapper 接口
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
@Mapper
public interface ArticleMapper extends BaseMapper<Article> {


}
