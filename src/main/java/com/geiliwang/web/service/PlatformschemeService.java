package com.geiliwang.web.service;

import com.geiliwang.web.entity.Platformscheme;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
public interface PlatformschemeService extends IService<Platformscheme> {

}
