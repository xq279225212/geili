package com.geiliwang.web.service;

import java.text.ParseException;
import java.util.List;
import java.util.Map;

/**
 * Created by chris on 2017/5/16.
 */
public interface SmsService {
    /**
     * Send sms string.
     *
     * @param phone          the 手机号
     * @param signName       the 短信签名
     * @param SMSTempateCode the 短信模版编码
     * @param params         the 模版参数
     * @return the string
     * @throws Exception the exception
     */
    String sendSms(String phone, String signName, String SMSTempateCode, Map<String, String> params) throws Exception;

    /**
     * Send sms vcode string.
     *
     * @param phone          the 手机号
     * @param signName       the 短信签名
     * @param SMSTempateCode the 短信模版编码
     * @param params         the 模版参数
     * @return the string
     * @throws Exception the exception
     */
    String sendSmsVcode(String phone, String signName, String SMSTempateCode, Map<String, String> params) throws Exception;

    /**
     * Check sms vcode boolean.
     *
     * @param messageId the 短信ID
     * @param userVcode the 用户提交短信验证码
     * @return the boolean
     * @throws ParseException the parse exception
     */
    boolean checkSmsVcode(String messageId, String userVcode) throws ParseException;


    /**
     * Check sms vcode boolean.
     *
     * @param messageId the 短信ID
     * @param userVcode the 用户提交短信验证码
     * @param phone the 发送验证码的手机号
     * @return the boolean
     * @throws ParseException the parse exception
     */
    boolean checkSmsVcode(String messageId, String userVcode, String phone) throws ParseException;


    /**
     * 短信失败记录
     * @param msgId
     * @param receiver
     * @param errorCode
     */
    void recordErrorMsg(String msgId, String receiver, String errorCode);

    /**
     * 批量发送短信
     * @param phones
     * @param signName
     * @param SMSTempateCode
     * @param params
     * @return
     * @throws Exception
     */
    String batchSendSms(List<String> phones, String signName, String SMSTempateCode, Map<String, String> params) throws Exception;

}
