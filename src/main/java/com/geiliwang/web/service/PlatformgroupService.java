package com.geiliwang.web.service;

import com.geiliwang.web.entity.PlatformGroupVO;
import com.geiliwang.web.entity.Platformgroup;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-06
 */
public interface PlatformgroupService extends IService<Platformgroup> {

    List<Platformgroup> findAll();

    /**
     * 查询分组与平台 信息
     * @return
     */
    List<PlatformGroupVO> findAllGroup();

}
