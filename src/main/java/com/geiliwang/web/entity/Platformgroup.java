package com.geiliwang.web.entity;

import com.baomidou.mybatisplus.enums.IdType;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author 邓
 * @since 2018-05-08
 */
public class Platformgroup implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 分组名称
     */
    @TableField("groupName")
    private String groupName;
    /**
     * 平台ids
     */
    @TableField("platformIds")
    private String platformIds;
    /**
     * 分组优先级
     */
    @TableField("groupOrder")
    private Integer groupOrder;
    /**
     * 删除标志
     */
    @TableField("delFlag")
    private String delFlag;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getPlatformIds() {
        return platformIds;
    }

    public void setPlatformIds(String platformIds) {
        this.platformIds = platformIds;
    }

    public Integer getGroupOrder() {
        return groupOrder;
    }

    public void setGroupOrder(Integer groupOrder) {
        this.groupOrder = groupOrder;
    }

    public String getDelFlag() {
        return delFlag;
    }

    public void setDelFlag(String delFlag) {
        this.delFlag = delFlag;
    }

    @Override
    public String toString() {
        return "Platformgroup{" +
        ", id=" + id +
        ", groupName=" + groupName +
        ", platformIds=" + platformIds +
        ", groupOrder=" + groupOrder +
        ", delFlag=" + delFlag +
        "}";
    }
}
