package com.geiliwang.web.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.geiliwang.web.entity.Platformarea;
import com.geiliwang.web.mapper.PlatformareaMapper;
import com.geiliwang.web.service.PlatformareaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Service
public class PlatformareaServiceImpl extends ServiceImpl<PlatformareaMapper, Platformarea> implements PlatformareaService {

    @Autowired
    private PlatformareaMapper platformareaMapper;

    @Override
    public List<Platformarea> selectAllNoDel() {
        EntityWrapper<Platformarea> ew = new EntityWrapper<>();
        ew.where("delFlag='0'");
        return platformareaMapper.selectList(ew);
    }
}
