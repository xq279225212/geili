package com.geiliwang.web.mapper;

import com.geiliwang.web.entity.Platform;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 邓
 * @since 2018-05-05
 */
@Mapper
public interface PlatformMapper extends BaseMapper<Platform> {

}
