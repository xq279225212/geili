package com.geiliwang.web.service;

import com.geiliwang.web.entity.FriendLink;
import com.baomidou.mybatisplus.service.IService;
import com.geiliwang.web.entity.FriendQuery;
import com.github.pagehelper.PageInfo;

import java.util.List;

/**
 * <p>
 * 友情链接 服务类
 * </p>
 *
 * @author 邓
 * @since 2018-05-03
 */
public interface FriendLinkService extends IService<FriendLink> {

    /**
     * 分页查询
     * @param query
     * @return
     */
    PageInfo<FriendLink> queryBy(FriendQuery query);

    /**
     * 查询使用的友情链接
     * @return
     */
    List<FriendLink> findUsedLink();

}
