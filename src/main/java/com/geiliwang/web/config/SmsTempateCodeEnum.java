package com.geiliwang.web.config;

public enum SmsTempateCodeEnum {
    SMS_TEST( "SMS_141560023", "信息变更验证码"),
    ;
    private final String code; //模版编号
    private final String name; //名称

    SmsTempateCodeEnum(String code, String name) {
        this.code = code;
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }
}
